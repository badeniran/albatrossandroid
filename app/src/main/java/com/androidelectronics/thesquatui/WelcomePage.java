package com.androidelectronics.thesquatui;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

public class WelcomePage extends AppCompatActivity {
    TextView appName;
    TextView logoName;
    AlphaAnimation appNameAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);
        setupWindowAnimations();
        appName = findViewById(R.id.appName);
        logoName = findViewById(R.id.companyName);

        // Setting up Squat animation
        appNameAnim  = new AlphaAnimation(0, 1);
        AlphaAnimation logoNameAnim = new AlphaAnimation(0, 1);

        appNameAnim.setDuration(1000);
        logoNameAnim.setDuration(1000);
        logoNameAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);
                            WelcomePage.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    appName.setText(R.string.app_name);
                                    appName.startAnimation(appNameAnim);
                                }
                            });
                        }
                        catch(InterruptedException e){
                        }
                    }
                };

                Thread t = new Thread(r);
                t.start();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        appNameAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                final Intent intent = new Intent(WelcomePage.this, OptionsScreen.class);
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(2000);
                            WelcomePage.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(WelcomePage.this).toBundle());
                                }
                            });
                        }
                        catch(InterruptedException e){
                        }
                    }
                };

                Thread t = new Thread(r);
                t.start();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        logoName.startAnimation(logoNameAnim);
    }

    private void setupWindowAnimations() {
        Slide slideExitTransition = new Slide(Gravity.RIGHT);
        slideExitTransition.excludeTarget(android.R.id.navigationBarBackground, true);
        slideExitTransition.excludeTarget(android.R.id.statusBarBackground, true);
        slideExitTransition.setDuration(1000);
        getWindow().setExitTransition(slideExitTransition);
    }
}
