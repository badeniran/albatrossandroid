package com.androidelectronics.thesquatui;

import android.app.ActivityOptions;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class HeatMap extends AppCompatActivity {
    ImageView tL;
    ImageView tR;
    ImageView bR;
    ImageView bL;
    View v;

    String receivedVal = "25252525";
    ArrayList<BoxColors> boxColors;

    boolean stop = false;
    Thread t;
    protected BluetoothDevice myBluetoothDevice;
    protected BluetoothGatt myGatt;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            // It means the user has changed his bluetooth state.
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {

                if (BluetoothAdapter.getDefaultAdapter().getState() == BluetoothAdapter.STATE_TURNING_OFF) {
                    // The user bluetooth is turning off yet, but it is not disabled yet.
                    bluetoothOffAlert();
                    return;
                }
                if (BluetoothAdapter.getDefaultAdapter().getState() == BluetoothAdapter.STATE_OFF) {
                    bluetoothOffAlert();
                    return;
                }

            }
        }
    };
    /* These constants are specific to a BLE GATT service */
    private static final UUID DATA_SERVICE_UUID = UUID.fromString("11223344-5566-7788-9900-aabbccddeeff");
    private static final UUID DATA_MEASUREMENT_UUID = UUID.fromString("11111111-1111-1111-1111-111111111111");

    /* This constant might be the same for all GATT services -- from android's sample LeGATT project */
    private static final UUID CLIENT_CHARACTERISTIC_CONFIG_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    /* This is the GATT callback that will be associated with the bluetooth device we are trying to connect to */
    protected BluetoothGattCallback myGattCallBack = new BluetoothGattCallback(){

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            System.err.println("Here within connection state change method. State: " + newState);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    //Upon connecting, now we must discover the services our device has to odder
                    //DEBUG -- System.err.println("We connected and now trying to discover");
                    gatt.discoverServices();
                }
                else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    //DEBUG -- System.err.println("Now disconnected");
                    stop = true;
                    Runnable x = new Runnable() {
                        @Override
                        public void run() {
                            while(t.isAlive());
                            HeatMap.this.runOnUiThread(new Runnable(){
                                @Override
                                public void run(){
                                    bluetoothOffAlert();
                                }
                            });
                        }
                    };
                    Thread a = new Thread(x);
                    a.start();

                }
                else {
                    //DEBUG -- System.err.println("Error! we are hanging...");
                }
            }
            /* These other cases are not necessary but are needed for debugging */
            else if(status == BluetoothGatt.GATT_FAILURE) {
                //DEBUG --  System.err.println("Connection attempt failed");
            }
            else {
                System.err.println("Random Connection error: " + status);
            }
        }

        //This is a callback function that will be triggered when new GATT services have been discovered
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status){
            if(status == BluetoothGatt.GATT_SUCCESS){
                System.err.println("Discovery successful");
                /*
                 * Set up GATT notification for data to be read
                 * On Discovering services, we want to enable GATT notifications for the data we would like to monitor
                 * - Start by getting the UUID of the characteristic we want to enable notifications for
                 */
                BluetoothGattCharacteristic dataCharacteristic =
                        gatt.getService(DATA_SERVICE_UUID)
                                .getCharacteristic(DATA_MEASUREMENT_UUID);

                /* - Enable notification for this characteristic
                 * - First setCharacteristicNotification to true
                 */
                gatt.setCharacteristicNotification(dataCharacteristic, true);
                /* Start thread to write characteristic*/
                Runnable r = new WriteGattCharacteristic(gatt, dataCharacteristic, CLIENT_CHARACTERISTIC_CONFIG_UUID);
                Thread a = new Thread(r);
                a.start();
            }
            else
                System.err.println("Discovery failed");
        }


        /* This is a call back function that will be triggered when the characteristic(data) we are trying to monitor changes */
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic){
            byte[] receivedData = characteristic.getValue();
            for(int i = 0; i < receivedData.length; i++){
                System.out.println(receivedData[i]);
            }
        }
    };

    public void bluetoothOffAlert(){
        new AlertDialog.Builder(HeatMap.this)
                .setTitle("Disconnected ")
                .setMessage("You need to be connected to a Squat device to view workout information")
                .setPositiveButton(" Reconnect", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Runnable x = new Runnable() {
                            @Override
                            public void run() {
                                final Intent initiateRescanIntent = new Intent(HeatMap.this, BLEScanActivity.class);
                                HeatMap.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        myGatt.close();
                                        startActivity(initiateRescanIntent, ActivityOptions.makeSceneTransitionAnimation(HeatMap.this).toBundle());
                                    }
                                });

                            }
                        };
                        Thread a = new Thread(x);
                        a.start();
                    }
                })
                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finished(v); //Do whatever we need to do next
                    }
                })
                .show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_heat_map);
        setupWindowAnimations();
        boxColors = new ArrayList<>();
        tL = findViewById(R.id.topLeft);
        bL = findViewById(R.id.bottomLeft);
        tR = findViewById(R.id.topRight);
        bR = findViewById(R.id.bottomRight);

        //Connect to selected bluetooth device
        this.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        myBluetoothDevice = (BluetoothDevice)getIntent().getParcelableExtra("mySelectedDevice");
        myGatt = myBluetoothDevice.connectGatt(HeatMap.this, true, myGattCallBack, 2);

        // Starting color change thread
        Runnable r = new ColorChange(this);
        t = new Thread(r);
        t.start();
    }


    public void finished(View view){
        stop = true;
        Runnable x = new Runnable() {
            @Override
            public void run() {
                while(t.isAlive());
                save();
                final Intent intent = new Intent(HeatMap.this, Replay.class);
                Bundle b = new Bundle();
                b.putSerializable("data", boxColors);
                intent.putExtra("CALL", 1);
                intent.putExtra("DATA", b);
                intent.putExtra("mySelectedDevice", myBluetoothDevice);
                myGatt.close();
                HeatMap.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(HeatMap.this).toBundle());
                    }
                });

            }
        };
        Thread a = new Thread(x);
        a.start();
    }

    private void save(){
        Date date = new Date();
        SavedWorkout savedWorkout = new SavedWorkout(boxColors, date);

        SharedPreferences mPref = getSharedPreferences(getString(R.string.preference_key), MODE_PRIVATE);
        Map<String, ?> entries = mPref.getAll();
        SharedPreferences.Editor editor = mPref.edit();
        Gson gson = new Gson();
        // Make sure there are 5 and they aren't null
        if (isFull(entries))
            deleteOldestData(entries, mPref, editor, gson);

        String json = gson.toJson(savedWorkout);
        editor.putString(savedWorkout.saveId, json);
        editor.commit();
    }

    private boolean isFull(Map<String, ?> entries){
        int count = 0;
        for (Map.Entry<String, ?> entry : entries.entrySet()) {
            if (entry.getValue() != null)
                count++;
        }
        if (count >= 5)
            return true;
        else
            return false;
    }

    private void deleteOldestData(Map<String, ?> entries, SharedPreferences prefs, SharedPreferences.Editor editor, Gson gson){
        String min = findMin(entries, prefs, gson);
        if(min == null)
            return;
        editor.remove(min);
        editor.commit();
    }

    public String findMin(Map<String, ?> entries, SharedPreferences prefs, Gson gson){
        String min = "ZZZ";
        for (Map.Entry<String, ?> entry : entries.entrySet()) {
            String key = entry.getKey();
            String json = prefs.getString(key, "");
            SavedWorkout obj;
            if (!json.equals("")) {
                obj = gson.fromJson(json, SavedWorkout.class);
                if (obj.saveId.compareTo(min) < 0)
                    min = obj.saveId;
            }
        }
        if (min.equals("ZZZ"))
            return null;
        return min;
    }

    @Override
    public void onBackPressed(){

    }

    private void setupWindowAnimations() {
        Slide slideExitTransition = new Slide(Gravity.LEFT);
        slideExitTransition.excludeTarget(android.R.id.navigationBarBackground, true);
        slideExitTransition.excludeTarget(android.R.id.statusBarBackground, true);
        slideExitTransition.setDuration(2000);
        getWindow().setExitTransition(slideExitTransition);
    }
}
