package com.androidelectronics.thesquatui;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SavedWorkout implements Serializable, Comparable<SavedWorkout> {
    ArrayList<BoxColors> workout;
    Date date;
    String tag;
    String saveId;

    public SavedWorkout(ArrayList<BoxColors> workout, Date date) {
        this.workout = workout;
        this.date = date;
        DateFormat yearTagFormat = new SimpleDateFormat("MMMM dd, yyyy");
        tag = yearTagFormat.format(date) + " at ";
        DateFormat timeTagFormat = new SimpleDateFormat("hh:mm a");
        tag += timeTagFormat.format(date);
        DateFormat idFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss:SSS");
        saveId = idFormat.format(date);
    }

    @Override
    public String toString() {
        return tag;
    }

    @Override
    public int compareTo(@NonNull SavedWorkout o) {
        if(this.tag.compareTo(o.tag) < 0)
            return 1;
        else
            return -1;
    }
}
