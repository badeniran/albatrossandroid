package com.androidelectronics.thesquatui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class PreviousWorkoutsScreen extends AppCompatActivity {
    ListView lv;
    ArrayList<SavedWorkout> arr = new ArrayList<> ();
    private AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView parent, View v, int position, long id) {
            // Do something in response to the click
            SavedWorkout sel = adapter.getItem(position);
            ArrayList<BoxColors> workout = sel.workout;
            Intent intent = new Intent(PreviousWorkoutsScreen.this, Replay.class);
            Bundle b = new Bundle();
            b.putSerializable("data", workout);
            intent.putExtra("CALL", 0);
            intent.putExtra("DATA", b);
            startActivity(intent);
        }
    };
    ArrayAdapter<SavedWorkout> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_workouts_screen);
        lv = findViewById(R.id.listView);
        TextView emptyText = findViewById(android.R.id.empty);
        lv.setEmptyView(emptyText);
        lv.setOnItemClickListener(mMessageClickedHandler);
        adapter = new ArrayAdapter<>(PreviousWorkoutsScreen.this, R.layout.listlayout, arr);
        lv.setAdapter(adapter);

        // Thread to load saved data
        Runnable r = new Runnable() {
            @Override
            public void run() {
                SharedPreferences mPref = getSharedPreferences(getString(R.string.preference_key), MODE_PRIVATE);
                Map<String, ?> entries = mPref.getAll();
                Gson gson = new Gson();
                for (Map.Entry<String, ?> entry : entries.entrySet()) {
                    if (entry.getValue() != null) {
                        String key = entry.getKey();
                        String json = mPref.getString(key, "");
                        if (!json.equals("")) {
                            SavedWorkout obj = gson.fromJson(json, SavedWorkout.class);
                            arr.add(obj);
                            Collections.sort(arr);
                            PreviousWorkoutsScreen.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(PreviousWorkoutsScreen.this, OptionsScreen.class);
        startActivity(intent);
    }
}
