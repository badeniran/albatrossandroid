package com.androidelectronics.thesquatui;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.view.View;

public class OptionsScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options_screen);
        setupWindowAnimations();
    }

    public void optionOne(View view){
        Intent intent = new Intent(this, HeatMapDemo.class);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    public void optionTwo(View view){
        Intent intent = new Intent(this, PreviousWorkoutsScreen.class);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    public void onBackPressed(){

    }

    private void setupWindowAnimations() {
        Fade anim = new Fade();
        anim.setDuration(1000);
        getWindow().setEnterTransition(anim);

        Explode explodeTransition = new Explode();
        explodeTransition.excludeTarget(android.R.id.navigationBarBackground, true);
        explodeTransition.excludeTarget(android.R.id.statusBarBackground, true);
        explodeTransition.setDuration(1000);
        getWindow().setExitTransition(explodeTransition);
    }
}
