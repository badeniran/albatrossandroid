package com.androidelectronics.thesquatui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

public class PairingScreen extends AppCompatActivity {
    TextView warningTextView;
    public final AlphaAnimation warningAnim = new AlphaAnimation(0, 1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pairing_screen);
        warningTextView = findViewById(R.id.pairingWarning);

        warningAnim.setDuration(500);

        // Will have to figure out what to do in terms of BT communication for go ahead, but this is a test
        // Will start pairing thread here
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    PairingScreen.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            warningTextView.setText(R.string.pairing_msg2);
                            warningTextView.startAnimation(warningAnim);
                        }
                    });
                    Thread.sleep(2000);
                    Intent workout = new Intent(PairingScreen.this, HeatMap.class);
                    startActivity(workout);
                }catch(InterruptedException e) {
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
    }
}
