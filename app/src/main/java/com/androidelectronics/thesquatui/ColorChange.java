package com.androidelectronics.thesquatui;

import android.content.Intent;
import android.graphics.Color;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by babaadeniran on 2018-03-27.
 */

public class ColorChange implements Runnable {
    String topLC;
    String topRC;
    String bottomLC;
    String bottomRC;
    HeatMap h;
    String errorColor = "#000000";
    String [] colors = {"00E5DC",
            "00E4D1",
            "00E3C6",
            "00E3BB",
            "00E2B1",
            "00E1A6",
            "00E19B",
            "00E090",
            "00E086",
            "00DF7B",
            "00DE70",
            "00DE66",
            "00DD5B",
            "00DD51",
            "00DC47",
            "00DB3C",
            "00DB32",
            "00DA28",
            "00DA1E",
            "00D914",
            "00D80A",
            "00D800",
            "09D700",
            "13D700",
            "1DD600",
            "27D500",
            "30D500",
            "3AD400",
            "43D400",
            "4DD300",
            "56D200",
            "60D200",
            "69D100",
            "72D100",
            "7CD000",
            "85CF00",
            "8ECF00",
            "97CE00",
            "A0CE00",
            "A9CD00",
            "B2CC00",
            "BBCC00",
            "C4CB00",
            "CBC900",
            "CABF00",
            "C9B500",
            "C9AB00",
            "C8A200",
            "C89800",
            "C78E00",
            "C68500",
            "C67B00",
            "C57200",
            "C56800",
            "C45F00",
            "C35600",
            "C34D00",
            "C24300",
            "C23A00",
            "C13100",
            "C02800",
            "C01F00",
            "BF1600",
            "BF0E00"};

    public ColorChange(HeatMap h){
        this.h = h;
    }

    private void setUpColors(){
        int tLC, tRC, bLC, bRC;
        try {
             tLC = Integer.valueOf(h.receivedVal.substring(0, 2));
             tRC = Integer.valueOf(h.receivedVal.substring(2, 4));
             bLC = Integer.valueOf(h.receivedVal.substring(4, 6));
             bRC = Integer.valueOf(h.receivedVal.substring(6));
        }catch(NumberFormatException | StringIndexOutOfBoundsException e){
            // Invalid data was received
            topLC = errorColor;
            topRC = errorColor;
            bottomLC = errorColor;
            bottomRC = errorColor;
            return;
        }

        topLC = "#";
        topLC += tLC >= colors.length ? colors[colors.length-1]:colors[tLC];
        topRC = "#";
        topRC += tRC >= colors.length ? colors[colors.length-1]:colors[tRC];
        bottomLC = "#";
        bottomLC += bLC >= colors.length ? colors[colors.length-1]:colors[bLC];
        bottomRC = "#";
        bottomRC += bRC >= colors.length ? colors[colors.length-1]:colors[bRC];
    }

    @Override
    public void run() {
        while(!h.stop) {
            setUpColors();
            if (h.boxColors.size() == 960) {
                h.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(h, "You've been working out too long! Finishing",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(4000);
                        } catch (InterruptedException e) {
                        }
                        h.finished(h.tL);
                    }
                };
                Thread t = new Thread(r);
                t.start();
                break;
            }

            h.boxColors.add(new BoxColors(topLC, topRC, bottomLC, bottomRC));
            h.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        h.tL.setBackgroundColor(Color.parseColor(topLC));
                        h.tR.setBackgroundColor(Color.parseColor(topRC));
                        h.bL.setBackgroundColor(Color.parseColor(bottomLC));
                        h.bR.setBackgroundColor(Color.parseColor(bottomRC));
                    }catch(Exception e){

                    }
                }
            });
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
    }
}
