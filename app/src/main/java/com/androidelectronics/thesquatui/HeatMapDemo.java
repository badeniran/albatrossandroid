package com.androidelectronics.thesquatui;

import android.app.ActivityOptions;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.transition.Explode;
import android.transition.Fade;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class HeatMapDemo extends FragmentActivity {
    private static final int NUM_PAGES = 7;
    FloatingActionButton fab;
    int prevPosition;
    int count;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heat_map_demo);
        setupWindowAnimations();
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HeatMapDemo.this, BLEScanActivity.class);
                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(HeatMapDemo.this).toBundle());
            }
        });
    }

    private void setupWindowAnimations() {
        Fade anim = new Fade();
        anim.setDuration(1000);
        getWindow().setEnterTransition(anim);

        Explode explodeTransition = new Explode();
        explodeTransition.excludeTarget(android.R.id.navigationBarBackground, true);
        explodeTransition.excludeTarget(android.R.id.statusBarBackground, true);
        explodeTransition.setDuration(1000);
        getWindow().setExitTransition(explodeTransition);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return Tutorial.create(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
