package com.androidelectronics.thesquatui;

import java.io.Serializable;

/**
 * Created by babaadeniran on 2018-03-27.
 */

public class BoxColors implements Serializable{
    String topLeft;
    String topRight;
    String bottomLeft;
    String bottomRight;

    public BoxColors(String topLeft, String topRight, String bottomLeft, String bottomRight) {
        this.topLeft = topLeft;
        this.topRight = topRight;
        this.bottomLeft = bottomLeft;
        this.bottomRight = bottomRight;
    }
}
