package com.androidelectronics.thesquatui;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Fade;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Iterator;

public class Replay extends AppCompatActivity {
    ImageView tL;
    ImageView tR;
    ImageView bR;
    ImageView bL;
    Button rp;
    Button ds;
    ArrayList<BoxColors> boxColors;
    boolean stop = false;
    int caller;

    Runnable r = new Runnable() {
        @Override
        public void run() {
            Iterator<BoxColors> it = boxColors.iterator();
            while (it.hasNext() && !stop) {
                final BoxColors c = it.next();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tL.setBackgroundColor(Color.parseColor(c.topLeft));
                        tR.setBackgroundColor(Color.parseColor(c.topRight));
                        bL.setBackgroundColor(Color.parseColor(c.bottomLeft));
                        bR.setBackgroundColor(Color.parseColor(c.bottomRight));
                    }
                });
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
            Replay.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rp.setVisibility(View.VISIBLE);
                    ds.setText("Done");
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_replay);
        setupWindowAnimations();
        Bundle data = getIntent().getBundleExtra("DATA");
        caller = getIntent().getIntExtra("CALL", -1);
        boxColors = (ArrayList<BoxColors>)data.getSerializable("data");
        rp = findViewById(R.id.restartButton);
        ds = findViewById(R.id.doneSkipButton);
        if (boxColors == null)
            System.out.println("Bad");
        else {
            rp.setVisibility(View.GONE);
            tL = findViewById(R.id.topLeft);
            bL = findViewById(R.id.bottomLeft);
            tR = findViewById(R.id.topRight);
            bR = findViewById(R.id.bottomRight);

            Thread t = new Thread(r);
            t.start();
        }
    }

    public void restart(View view){
        Thread a = new Thread(r);
        rp.setVisibility(View.GONE);
        ds.setText("Skip");
        a.start();
    }

    public void doneRep(View view){
        stop = true;
        Intent intent;
        if (caller == 0){
            intent = new Intent(Replay.this, PreviousWorkoutsScreen.class);
        }
        else{
            intent = new Intent(Replay.this, PostWorkoutScreen.class);
        }
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){

    }

    private void setupWindowAnimations() {
        Fade anim = new Fade();
        anim.setDuration(1000);
        getWindow().setEnterTransition(anim);
    }
}
