package com.androidelectronics.thesquatui;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

import java.util.UUID;

public class WriteGattCharacteristic implements Runnable {
    BluetoothGatt gatt;
    BluetoothGattCharacteristic dataCharacteristic;
    UUID id;

    public WriteGattCharacteristic(BluetoothGatt gatt, BluetoothGattCharacteristic dataCharacteristic, UUID id) {
        this.gatt = gatt;
        this.dataCharacteristic = dataCharacteristic;
        this.id = id;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        /* - Then do a necessary descriptor write as required by BLE protocols */
        BluetoothGattDescriptor descriptor = dataCharacteristic.getDescriptor(id);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        if(gatt.writeDescriptor(descriptor))
            System.err.println("Notifications enabled");
        else
            System.err.println("Notifications not enabled");
    }
}
