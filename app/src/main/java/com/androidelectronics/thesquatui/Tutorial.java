package com.androidelectronics.thesquatui;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class Tutorial extends Fragment {
    public static final String ARG_PAGE = "page";
    private int mPageNumber;

    public static Tutorial create(int pageNumber) {
        Tutorial fragment = new Tutorial();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public Tutorial() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE) + 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup rootView;
        switch (mPageNumber){
            case 1:
                rootView = (ViewGroup) inflater
                        .inflate(R.layout.fragment_demo1, container, false);
                ((TextView) rootView.findViewById(R.id.tut_title1)).setText(
                        getString(R.string.tutorial_title_template, mPageNumber));
                break;
            case 2:
                rootView = (ViewGroup) inflater
                        .inflate(R.layout.fragment_demo2, container, false);
                ((TextView) rootView.findViewById(R.id.tut_title2)).setText(
                        getString(R.string.tutorial_title_template, mPageNumber));
                break;
            case 3:
                rootView = (ViewGroup) inflater
                        .inflate(R.layout.fragment_demo3, container, false);
                ((TextView) rootView.findViewById(R.id.tut_title3)).setText(
                        getString(R.string.tutorial_title_template, mPageNumber));
                break;
            case 4:
                rootView = (ViewGroup) inflater
                        .inflate(R.layout.fragment_demo4, container, false);
                ((TextView) rootView.findViewById(R.id.tut_title4)).setText(
                        getString(R.string.tutorial_title_template, mPageNumber));
                break;
            case 5:
                rootView = (ViewGroup) inflater
                        .inflate(R.layout.fragment_demo5, container, false);
                ((TextView) rootView.findViewById(R.id.tut_title5)).setText(
                        getString(R.string.tutorial_title_template, mPageNumber));
                break;
            case 6:
                rootView = (ViewGroup) inflater
                        .inflate(R.layout.fragment_demo6, container, false);
                ((TextView) rootView.findViewById(R.id.tut_title6)).setText(
                        getString(R.string.tutorial_title_template, mPageNumber));
                break;
            case 7:
                rootView = (ViewGroup) inflater
                        .inflate(R.layout.fragment_demo7, container, false);
                ((TextView) rootView.findViewById(R.id.tut_title7)).setText(
                        getString(R.string.tutorial_title_template, mPageNumber));
                break;
            default:
                rootView = (ViewGroup) inflater
                        .inflate(R.layout.fragment_demo1, container, false);
                ((TextView) rootView.findViewById(R.id.tut_title1)).setText(
                        getString(R.string.tutorial_title_template, 1));
        }
        return rootView;
    }

}
