package com.androidelectronics.thesquatui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This is a program that scans for BLE devices with a certain name, and displays them in a list for the
 * user to make a selection
 * @author Abdulkareem Dolapo
 * @version 1.0
 * @since March 27, 2018
 */
public class BLEScanActivity extends ListActivity {
    /*
    TODO -- Notes: ListView(must have a specific ID for ListActivity) is already created in content view
    TODO    of this activity. Also a TextView(for when list is empty - specific ID also) has been created
    */

    /*
     * A BluetoothAdapter is a class that represents your phones bluetooth connection
     */
    private BluetoothAdapter myBluetoothAdapter;
    /*
     * A Handler handles running of threads
     */
    private Handler myHandler;
    /*
     * LeDeviceListAdapter is a class that we build (see further below), used to populate list view
     */
    private LeDeviceListAdapter myDevicesListAdapter;
    /*
     * List of ScanFilters and a ScanFilter.Builder required to limit devices we scan for
     */
    private final ScanFilter.Builder scanFilterBuilder = new ScanFilter.Builder()
                                                        .setDeviceName("AlbatrossLink_CBCB"); //"AlbatrossLink_CBCB" "RN4870-0000"
    private final ArrayList<ScanFilter> scanFilter = new ArrayList<ScanFilter>();
    /*
     * ScanSettings.Builder and ScanSettings object required to perform a custom BLE scan
     */
    private ScanSettings.Builder scanSettingsBuilder = new ScanSettings.Builder()
                                                            .setReportDelay(0)
                                                            .setScanMode(ScanSettings.SCAN_MODE_BALANCED);
    private ScanSettings scanSettings;
    /*
     * Boolean indicating if a scan is in progress
     */
    private boolean isScanning;
    private Button reScanButton;
    private TextView reScanText;
    //Constants
    private final int REQUEST_ENABLE_BT = 1; //Code for requesting BT to be enabled
    private final int SCAN_PERIOD = 10000; //Time we scan for
    static public final int REQUEST_LOCATION = 2; // Code for requesting location

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blescan);

        //setupWindowAnimations();
        reScanButton = findViewById(R.id.button);
        reScanText = findViewById(R.id.textView);
        reScanButton.setVisibility(View.GONE);
        reScanText.setText("Scanning");
        //Check if device supports bluetooth, if not exut
        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            new AlertDialog.Builder(this)
                    .setTitle("Not Compatible")
                    .setMessage("This device does not support BLE")
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })
                    .show();
        }

        //Perform necessary initializations
        scanFilter.add(scanFilterBuilder.build()); //Add our filter to the list of scan filters
        scanSettings = scanSettingsBuilder.build();
        myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        myHandler = new Handler();

        myDevicesListAdapter = new LeDeviceListAdapter();
        setListAdapter(myDevicesListAdapter);

        //Enable BT if not already enabled then scan
        if(myBluetoothAdapter == null || !myBluetoothAdapter.isEnabled()){
           Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
           startActivityForResult(enableBT, REQUEST_ENABLE_BT);
        }
        else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
            }
            else{
                scanLeDevices(true);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        //This is called when a result has been received from our request to enable BT
        if(requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED){
            new AlertDialog.Builder(this)
                    .setTitle("Bluetooth Required")
                    .setMessage("Bluetooth must be enabled to communicate with a Squat device")
                    .setPositiveButton("Continue", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int which){
                            finish();
                        }
                    })
                    .show();
        }
        else{
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
            }
            else{
                scanLeDevices(true);
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if(requestCode == REQUEST_LOCATION){
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                scanLeDevices(true);
            }
            else{
                new AlertDialog.Builder(this)
                        .setTitle("Location Required")
                        .setMessage("Location permission must be granted communicate with a Squat device")
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int which){
                                finish();
                            }
                        })
                        .show();
            }
        }
    }

    public void scanLeDevices(final boolean enable){

        if(isScanning){ return; }

        //First clear list
        if(!myDevicesListAdapter.isEmpty()){
            runOnUiThread(new Runnable(){
                @Override
                public void run(){
                    myDevicesListAdapter.clear();
                    myDevicesListAdapter.notifyDataSetChanged();
                }
            });
        }

        //Enable BT if not already enabled then scan
        if((!myBluetoothAdapter.isEnabled())) {
            Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBT, REQUEST_ENABLE_BT);
        }


        //Require an LE Bluetooth Scanner to scan
        final BluetoothLeScanner myBluetoothLeScanner = myBluetoothAdapter.getBluetoothLeScanner();
        if(enable){
            //Launch thread to kill scanning process after SCAN_PERIOD
            myHandler.postDelayed(new Runnable(){
                @Override
                public void run(){
                    isScanning = false;
                    myBluetoothLeScanner.stopScan(myLeScanCallback);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            reScanButton.setVisibility(View.VISIBLE);
                            reScanText.setText("Can't find your device?");
                        }
                    });

                }
            }, SCAN_PERIOD);
            isScanning = true;
            myBluetoothLeScanner.startScan(scanFilter, scanSettings, myLeScanCallback); //fix
            //myBluetoothLeScanner.startScan(myLeScanCallback);
        }
        else{
            isScanning = false;
            myBluetoothLeScanner.stopScan(myLeScanCallback);
        }
    }

    //ScanCallback required to perform scan
    private ScanCallback myLeScanCallback = new ScanCallback(){
        /*
        Function handles situation where multiple devices were found at the same time
        ScanResult is a class that holds information about the bluetooth device. The
        Bluetooth device is pareceled (packaged) within ScanResult
        */
        @Override
        public void onBatchScanResults(List<ScanResult> results){
            //Add results to list, update UI as devices are found
            final Iterator iterate = results.iterator();
            runOnUiThread(new Runnable(){
                ScanResult result;
                @Override
                public void run(){
                    while(iterate.hasNext()){
                        result = (ScanResult)iterate.next();
                        myDevicesListAdapter.addDevice(result.getDevice());
                    }
                    myDevicesListAdapter.notifyDataSetChanged();
                }
            });
        }

        //Function handles situation where a single device was found
        @Override
        public  void onScanResult(int callbackType, ScanResult result){
            //Fix -- Add result to list
            final BluetoothDevice device = result.getDevice();
            runOnUiThread(new Runnable(){
                @Override
                public void run(){
                    myDevicesListAdapter.addDevice(device);
                    myDevicesListAdapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onScanFailed(int errorCode) {
            System.err.println("Here in onScanFailed. Error Code: " + errorCode);
            new AlertDialog.Builder(BLEScanActivity.this)
                    .setTitle("Scan failed")
                    .setMessage("Make sure this app has permission to access device location")
                    .setPositiveButton("Continue", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int which){
                            //Do nothing
                        }
                    })
                    .show();
        }
    };

    /*
    * A ListView is a ViewGroup that displays a list of vertically scrollable items. The list
      items are automatically inserted into a list using an "adapter" --
    * An Adapter is used to insert things into lists from a source such an ArrayList
    * BaseAdapter is an abstract class for many concrete adapter classes
    * This class will be used to hold all BLE devices found
    */
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator; //Will reference layout inflater of this activity

        public LeDeviceListAdapter(){
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = BLEScanActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device){
            if(!mLeDevices.contains(device)){
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position){
            return mLeDevices.get(position);
        }

        public void clear(){
            mLeDevices.clear();
        }

        @Override
        public int getCount(){return mLeDevices.size();}

        @Override
        public Object getItem(int i){ return mLeDevices.get(i);} //Redundant but necessary

        @Override
        public long getItemId(int i){ return i;}

        @Override
        public View getView(int i, View view, ViewGroup viewGroup){
            /*
            i - position in list adapter
            view - a view object that will be inserted into list, it's passed in as an argument, will
            also be passed out
            viewGroup - parent
            */

            ViewHolder viewHolder;
            if(view == null){
                view = mInflator.inflate(R.layout.listitems, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView)view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView)view.findViewById(R.id.device_name);
                view.setTag(viewHolder); //Attach viewHolder to view to be displayed in list
            }
            else{
                viewHolder = (ViewHolder)view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if(deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText("Unknown device");
            viewHolder.deviceAddress.setText(device.getAddress());

            return view;
        }
    }

    /* ActionListener for this activity's ListView */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        final BluetoothDevice device = myDevicesListAdapter.getDevice(position);
        if(isScanning)
            scanLeDevices(false); //Stop scanning
        if(device == null) return;
        //Make an intent to start a new activity, and attach found device to the activity
        Intent communicateActivityIntent = new Intent(this, HeatMap.class);
        communicateActivityIntent.putExtra("mySelectedDevice", device);
        startActivity(communicateActivityIntent);
        finish();
    }

    /*
    ActionListener function for "Rescan" button -- see xml and notice it has been linked
    to the buttons "onClick" field.
    */
    public void reScanButtonActionFunction(View view){
        //If already done scanning, then rescan
        if(!isScanning) {
            reScanButton.setVisibility(View.GONE);
            reScanText.setText("Scanning");
            scanLeDevices(true);
        }
    }
    static class ViewHolder{
        TextView deviceName;
        TextView deviceAddress;
    }

}
