package com.androidelectronics.thesquatui;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.Window;

public class PostWorkoutScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupWindowAnimations();
        setContentView(R.layout.activity_post_workout_screen);
    }

    public void optionOne(View view){
        Intent intent = new Intent(this, OptionsScreen.class);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    public void optionTwo(View view){
        Intent intent = new Intent(this, HeatMap.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){

    }

    private void setupWindowAnimations() {
        Explode slideExitTransition = new Explode();
        slideExitTransition.excludeTarget(android.R.id.navigationBarBackground, true);
        slideExitTransition.excludeTarget(android.R.id.statusBarBackground, true);
        slideExitTransition.setDuration(2000);
        getWindow().setExitTransition(slideExitTransition);
    }
}
